LibVLC sample for Android
========================

This is a sample application for using libVLC on Android.

This repository has sample code and a built-in libVLC(Version 1.9.7) that can be built and replaced into libvlc folder.

Getting started for building libVLC
-----------------------------------
Requirements:

* An up-to-date Linux distribution.
* Android NDK r11c
* Android SDK
* Android Studio
* [Other requirements](https://wiki.videolan.org/AndroidCompile#Requirements) listed in the compile guide.

1. First build VLC for Android, by following [AndroidCompile](https://wiki.videolan.org/AndroidCompile) instructions.
2. Copy ```libvlc/build/outputs/aar/libvlc-3.0.0-1.9.7.aar``` to the libvlc folder and **rename** it to ```libvlc-3.0.0.aar```.
3. Build application in Android Studio.

The APK build output can be found under ```build/outputs/apk/```.