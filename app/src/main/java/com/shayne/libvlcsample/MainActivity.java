package com.shayne.libvlcsample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEditTextUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        initUI();
    }

    private void initUI() {
        mEditTextUrl = (EditText) findViewById(R.id.edittext_url);
        Button btnPlay = (Button) findViewById(R.id.btn_play);
        btnPlay.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_play:
                playUrl(mEditTextUrl.getText().toString().trim());
                break;
        }
    }

    private void playUrl(String url) {
        Intent intent = new Intent(this, PlayerActivity.class);
        intent.putExtra(getString(R.string.url), url);
        startActivity(intent);
    }
}
